# pika-collectd

Create the collectd RPM package for PIKA, just run.

### Adjust Settings

```
cp pika-collectd-template.conf pika-collectd.conf
```

Edit sections `pika-collectd.conf` to you system
```
  Import "influx_write"
  <Module influx_write>
    #host
    #port
    #user
    #pwd
    #database

  Import "ib_bw"
  <Module ib_bw>
    #devices "/sys/class/infiniband/mlx4_0"
    #directory "/sys/class/infiniband"
    recheck_limit 1440
  </Module>

  Import "lustre_bw"
  <Module lustre_bw>
    #path "Lustre instance paths (comma separated)"
    #fsname_and_mount "instance:mount point (comma separated)"
    recheck_limit 360 # every 3h
```

### Build Package

```sh
./build.sh
```
