#! /bin/bash

source ../rpm.conf

build_docker_image() {
    if [[ $(docker images -q rpmbuild) == "" ]]; then
        # adjust release version for rpm builds
        sed -i "s/RELEASE_VERSION=.*/RELEASE_VERSION=${RELEASE_VERSION}/" ../rpmbuild-docker/docker-rpm-build.sh
        docker build -f $(pwd)/../rpmbuild-docker/Dockerfile.${OS}${OS_VERSION%%.*} --build-arg OS_VERSION=${OS_VERSION} -t rpmbuild "$(pwd)/../rpmbuild-docker/"
    fi
}


get_sources() {
    local source="https://storage.googleapis.com/collectd-tarballs/collectd-5.12.0.tar.bz2"
    if ! wget --no-check-certificate -c "${source}"; then
        echo "ERROR could not fetch sources: ${source}"
        exit 1
    fi
}


run_docker_rpmbuild() {
    build_docker_image
    if [ "${OS_VERSION%%.*}" -ge 9 ]; then
        docker run --rm --volume="$(pwd)":/src --workdir=/src rpmbuild pika-collectd-el${OS_VERSION%%.*}.spec
    else
        docker run --rm --volume="$(pwd)":/src --workdir=/src rpmbuild pika-collectd.spec
    fi
}


get_sources
run_docker_rpmbuild
