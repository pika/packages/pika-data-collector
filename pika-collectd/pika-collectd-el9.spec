%global _hardened_build 1
%global __provides_exclude_from ^%{_libdir}/collectd/.*\\.so$

%define collectd_name collectd
%define _unpackaged_files_terminate_build 0
%global debug_package %{nil}

Summary: Statistics collection daemon for filling RRD files
Name: pika-collectd
Version: 5.12.0
Release: %{?release_version}%{?dist}
License: GPLv2
Group: System Environment/Daemons
URL: https://collectd.org/

Source: %{collectd_name}-%{version}.tar.bz2
Source1: pika-collectd.conf
Source2: ib_bw.py
Source3: influx_write.py
Source4: lustre_bw.py
Source5: custom_types.db
Source6: systemd.pika-collectd.service
Source7: pika.logrotate
Source8: prolog.sh

BuildRequires: perl(ExtUtils::MakeMaker)
BuildRequires: perl(ExtUtils::Embed)
BuildRequires: python3-devel
BuildRequires: libgcrypt-devel
BuildRequires: kernel-headers
BuildRequires: libcap-devel
BuildRequires: which
BuildRequires: gcc
BuildRequires: xfsprogs-devel
BuildRequires: systemd
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd
Requires: logrotate

%if 0%{?rhel} >= 8
Requires: python3-influxdb
%endif

Patch0: collectd-5.12.0_daemon.patch

%description
collectd is a daemon which collects system performance statistics periodically
and provides mechanisms to store the values in a variety of ways,
for example in RRD files.

%package amqp
Summary:	AMQP 0.9 plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
BuildRequires:	librabbitmq-devel
%description amqp
The AMQP 0.9 plugin transmits or receives values collected by collectd via the
Advanced Message Queuing Protocol v0.9 (AMQP).


%package amqp1
Summary:	AMQP 1.0 plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
BuildRequires:	qpid-proton-c-devel
%description amqp1
The AMQP 1.0 plugin transmits or receives values collected by collectd via the
Advanced Message Queuing Protocol v1.0 (AMQP1).


%package apache
Summary:	Apache plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
BuildRequires:	curl-devel
%description apache
This plugin collects data provided by Apache's `mod_status'.


%package bind
Summary:	Bind plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
BuildRequires:	libxml2-devel, curl-devel
%description bind
The BIND plugin retrieves this information that's encoded in XML and provided
via HTTP and submits the values to collectd.


%package ceph
Summary:       Ceph plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: yajl-devel
%description ceph
This plugin collects data from Ceph.


%package chrony
Summary:       Chrony plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
%description chrony
Chrony plugin for collectd


%package connectivity
Summary:       Connectivity plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: libmnl-devel, yajl-devel
%description connectivity
Monitors network interface up/down status via netlink library.


%package curl
Summary:       Curl plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: curl-devel
%description curl
This plugin reads webpages with curl


%package curl_json
Summary:       Curl JSON plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: curl-devel
BuildRequires: yajl-devel
%description curl_json
This plugin retrieves JSON data via curl.


%package curl_xml
Summary:       Curl XML plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: curl-devel
BuildRequires: libxml2-devel
%description curl_xml
This plugin retrieves XML data via curl.


%package disk
Summary:	disk plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
%{?_has_libudev:BuildRequires:  libudev-devel}
%description disk
The "disk" plugin collects information about the usage of physical disks and
logical disks (partitions).


%package dns
Summary:	DNS plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}, libpcap >= 1.0
BuildRequires:	libpcap-devel >= 1.0
%description dns
The DNS plugin has a similar functionality to dnstop: It uses libpcap to get a
copy of all traffic from/to port UDP/53 (that's the DNS port), interprets the
packets and collects statistics of your DNS traffic.


%package email
Summary:       Email plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
%description email
This plugin collects data provided by spamassassin.


%package hddtemp
Summary:	Hddtemp plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}, hddtemp
%description hddtemp
The HDDTemp plugin collects the temperature of hard disks. The temperatures are
provided via SMART and queried by the external hddtemp daemon.


%package ipmi
Summary:       IPMI plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: OpenIPMI-devel
%description ipmi
This plugin for collectd provides IPMI support.


%package log_logstash
Summary:       log_logstash plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: yajl-devel
%description log_logstash
This plugin logs in logstash JSON format

%package lua
Summary:       Lua plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: lua-devel
%description lua
The Lua plugin embeds a Lua interpreter into collectd and exposes the
application programming interface (API) to Lua scripts.


%package mcelog
Summary:       Mcelog plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
%description mcelog
This plugin monitors machine check exceptions reported by mcelog and generates
appropriate notifications when machine check exceptions are detected.


%package memcachec
Summary:       Memcachec plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: libmemcached-devel
%description memcachec
This plugin connects to a memcached server, queries one or more
given pages and parses the returned data according to user specification.


%package mqtt
Summary:       MQTT plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: mosquitto-devel
%description mqtt
The MQTT plugin publishes and subscribes to MQTT topics.


%package mysql
Summary:       MySQL plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: mysql-devel
%description mysql
MySQL querying plugin. This plugin provides data of issued commands,
called handlers and database traffic.


%package netlink
Summary:	netlink plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
BuildRequires:	libmnl-devel, iproute-devel
%description netlink
The netlink plugin collects detailed network interface and routing statistics.


%package nginx
Summary:	Nginx plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
BuildRequires:	curl-devel
%description nginx
This plugin gets data provided by nginx.


%package notify_email
Summary:       Notify email plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: libesmtp-devel
%description notify_email
This plugin uses the ESMTP library to send
notifications to a configured email address.


%ifnarch s390 s390x
%package nut
Summary:       Network UPS Tools plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: nut-devel
%description nut
This plugin for collectd provides Network UPS Tools support.
%endif


%package openldap
Summary:       OpenLDAP plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: openldap-devel
%description openldap
This plugin for collectd reads monitoring information
from OpenLDAP's cn=Monitor subtree.


%package ovs_events
Summary:       Open vSwitch events plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: yajl-devel
%description ovs_events
This plugin monitors the link status of Open vSwitch (OVS) connected
interfaces, dispatches the values to collectd and sends notifications
whenever a link state change occurs in the OVS database.


%package ovs_stats
Summary:       Open vSwitch stats plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: yajl-devel
%description ovs_stats
This plugin collects statictics of OVS connected bridges and interfaces.


%package perl
Summary:	Perl plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
BuildRequires:	perl-ExtUtils-Embed
%description perl
The Perl plugin embeds a Perl interpreter into collectd and exposes the
application programming interface (API) to Perl-scripts.


%package pcie_errors
Summary:	PCI Express errors plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
%description pcie_errors
The pcie_errors plugin collects PCI Express errors from Device Status in Capability
structure and from Advanced Error Reporting Extended Capability.


%package ping
Summary:       Ping plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: liboping-devel
%description ping
This plugin for collectd provides network latency statistics.


%package postgresql
Summary:       PostgreSQL plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: postgresql-devel
%description postgresql
PostgreSQL querying plugin. This plugins provides data of issued commands,
called handlers and database traffic.


%package procevent
Summary:       Processes event plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: yajl-devel
%description procevent
Monitors process starts/stops via netlink library.

%package python
Summary:	Python plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
BuildRequires: python3-devel
%description python
The Python plugin embeds a Python interpreter into collectd and exposes the
application programming interface (API) to Python-scripts.

%package redis
Summary:       Redis plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: hiredis-devel
%description redis
The Redis plugin connects to one or more instances of Redis, a key-value store,
and collects usage information using the hiredis library.


%package rrdcached
Summary:       RRDCacheD plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: rrdtool-devel
%description rrdcached
This plugin uses the RRDtool accelerator daemon, rrdcached(1),
to store values to RRD files in an efficient manner.


%package rrdtool
Summary:       RRDTool plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: rrdtool-devel
%description rrdtool
This plugin for collectd provides rrdtool support.


%ifnarch ppc sparc sparc64 aarch64
%package sensors
Summary:       Libsensors module for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: lm_sensors-devel
%description sensors
This plugin for collectd provides querying of sensors supported by
lm_sensors.
%endif


%package snmp
Summary:       SNMP module for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: net-snmp-devel
%description snmp
This plugin for collectd provides querying of net-snmp.


%package snmp_agent
Summary:       SNMP AgentX plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: net-snmp-devel
%description snmp_agent
This plugin is an AgentX subagent that receives and handles queries
from a SNMP master agent and returns the data collected by read plugins.


%package sysevent
Summary:       Rsyslog event plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: yajl-devel
%description sysevent
Monitors rsyslog for system events.


%package write_http
Summary:	Write-HTTP plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
BuildRequires:	curl-devel
%description write_http
The Write-HTTP plugin sends the values collected by collectd to a web-server
using HTTP POST requests.


%package write_redis
Summary:       Redis output plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
BuildRequires: hiredis-devel
%description write_redis
This plugin can send data to Redis.


%package write_syslog
Summary:	write_syslog plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
%description write_syslog
The write_syslog collectd plugin writes metrics to syslog
using JSON or RFC5424 formatting


%package write_tsdb
Summary:       OpenTSDB output plugin for collectd
Group:         System Environment/Daemons
Requires:      %{name}%{?_isa} = %{version}-%{release}
%description write_tsdb
This plugin can send data to OpenTSDB.


%package collection3
Summary:	Web-based viewer for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires: httpd
%description collection3
collection3 is a graphing front-end for the RRD files created by and filled
with collectd. It is written in Perl and should be run as an CGI-script.
Graphs are generated on-the-fly, so no cron job or similar is necessary.


%package php-collection
Summary:	collect php webfrontent
Group:		System Environment/Daemons
Requires:	collectd = %{version}-%{release}
Requires:	httpd
Requires:	php
Requires:	php-rrdtool
%description php-collection
PHP graphing frontend for RRD files created by and filled with collectd.


%package contrib
Summary:	Contrib files for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
%description contrib
All the files found under contrib/ in the source tree are bundled in this


%package -n libcollectdclient
Summary:	Collectd client library
Group:		System Environment/Daemons
%description -n libcollectdclient
Collectd client library


%package -n libcollectdclient-devel
Summary:	Development files for libcollectdclient
Group:		System Environment/Daemons
Requires:	pkgconfig
Requires:	libcollectdclient%{?_isa} = %{version}-%{release}
%description -n libcollectdclient-devel
Development files for libcollectdclient


%package -n collectd-utils
Summary:	Collectd utilities
Group:		System Environment/Daemons
Requires:	libcollectdclient%{?_isa} = %{version}-%{release}
Requires:	collectd%{?_isa} = %{version}-%{release}
%description -n collectd-utils
Collectd utilities

%package gpu_nvidia
Summary:	stackdriver plugin for collectd
Group:		System Environment/Daemons
Requires:	%{name}%{?_isa} = %{version}-%{release}
BuildRequires: cuda-nvml-devel-12-4
%description gpu_nvidia
The gpu_nvidia collectd plugin collects NVidia GPU metrics.

%prep
%setup -q -n collectd-5.12.0
%patch0 -p0 -d src/

%build
%configure CFLAGS="%{optflags} -DLT_LAZY_OR_NOW=\"RTLD_LAZY|RTLD_GLOBAL\"" \
    LDFLAGS="-Wl,-rpath=/usr/lib64/%{name} %{optflags}" \
    --disable-dbi \
    --disable-iptables \
    --disable-snmp \
    --disable-snmp_agent \
    --disable-dependency-tracking \
    --disable-silent-rules \
    --enable-all-plugins=yes \
    --disable-static \
    --enable-match_empty_counter \
    --enable-match_hashed \
    --enable-match_regex \
    --enable-match_timediff \
    --enable-match_value \
    --enable-target_notification \
    --enable-target_replace \
    --enable-target_scale \
    --enable-target_set \
    --enable-target_v5upgrade \
    --disable-apple_sensors \
    --disable-aquaero \
    --disable-barometer \
    --disable-dpdkevents \
    --disable-dpdkstat \
    --disable-dpdk_telemetry \
    --disable-grpc \
    --disable-intel_pmu \
    --disable-intel_rdt \
    --disable-battery \
    --disable-ascent \
    --disable-ganglia \
    --disable-gmond \
    --disable-gps \
    --disable-modbus \
    --disable-notify_desktop \
    --disable-varnish \
    --disable-virt \
    --disable-write_kafka \
    --disable-write_mongodb \
    --disable-write_riemann \
    --disable-write_sensu \
    --disable-pinba \
    --disable-write_stackdriver \
    --disable-slurm \
    --disable-redfish \
    --disable-dcpmm \
    --disable-capabilities \
    --disable-ipstats \
    --disable-write_prometheus \
    --disable-lpar \
    --disable-mic \
    --disable-netapp \
    --with-cuda=/usr/local/cuda-12.4/targets/x86_64-linux \
    --enable-gpu_nvidia \
    --disable-smart \
    --disable-netstat_udp \
    --disable-zookeeper \
    --disable-zfs_arc \
    --disable-wireless \
    --disable-vserver \
%ifarch s390 s390x
    --disable-nut \
%endif
    --disable-onewire \
    --disable-oracle \
    --disable-pf \
    --disable-routeros \
%ifarch ppc sparc sparc64 aarch64
    --disable-sensors \
%endif
    --disable-sigrok \
    --disable-tape \
    --disable-tokyotyrant \
%ifnarch %ix86 x86_64
    --disable-turbostat \
%endif
    --disable-xencpu \
    --disable-xmms \
    --disable-zone \
    --disable-java \
    --with-perl-bindings=INSTALLDIRS=vendor \
    --disable-werror

make %{?_smp_mflags}


%install
make install DESTDIR="%{buildroot}"

mv %{buildroot}%{_sysconfdir}/%{collectd_name}.conf %{buildroot}%{_sysconfdir}/%{name}.conf
install -Dp -m0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/%{name}.conf
install -Dp -m0644 %{SOURCE6} %{buildroot}%{_unitdir}/%{name}.service
install -Dp -m0644 %{SOURCE7} %{buildroot}%{_sysconfdir}/logrotate.d/pika
install -Dp -m0644 %{SOURCE5} %{buildroot}%{_datadir}/%{name}/custom_types.db
install -d %{buildroot}%{_datadir}/%{name}/plugins/
install -Dp -m0644 %{SOURCE2} %{buildroot}%{_datadir}/%{name}/plugins/ib_bw.py
install -Dp -m0644 %{SOURCE3} %{buildroot}%{_datadir}/%{name}/plugins/influx_write.py
install -Dp -m0644 %{SOURCE4} %{buildroot}%{_datadir}/%{name}/plugins/lustre_bw.py
install -d %{buildroot}%{_sharedstatedir}/%{name}/
install -d %{buildroot}%{_sysconfdir}/%{name}.d/
install -Dp -m0755 %{SOURCE8} %{buildroot}/etc/pika/prolog.sh

# *.la files shouldn't be distributed.
rm -f %{buildroot}/%{_libdir}/{%{collectd_name}/,}*.la

# Remove Perl hidden .packlist files.
find %{buildroot} -type f -name .packlist -delete
# Remove Perl temporary file perllocal.pod
find %{buildroot} -type f -name perllocal.pod -delete

# delete java crap, we don't need it.
rm -f %{buildroot}%{_datadir}/%{collectd_name}/java/%{collectd_name}-api.jar
rm -f %{buildroot}%{_datadir}/%{collectd_name}/java/generic-jmx.jar
rm -f %{buildroot}%{_mandir}/man5/%{collectd_name}-java.5*

# prefix pika- to directories and files
mkdir -p %{buildroot}%{_libdir}/%{name}
mkdir -p %{buildroot}%{_includedir}/%{name}
mkdir -p %{buildroot}%{_datadir}/%{name}
mv %{buildroot}%{_libdir}/%{collectd_name}/* %{buildroot}%{_libdir}/%{name}/
mv %{buildroot}%{_includedir}/%{collectd_name}/* %{buildroot}%{_includedir}/%{name}/
mv %{buildroot}%{_datadir}/%{collectd_name}/* %{buildroot}%{_datadir}/%{name}/

mv %{buildroot}%{_sbindir}/%{collectd_name} %{buildroot}%{_sbindir}/%{name}
mv %{buildroot}%{_sbindir}/%{collectd_name}mon %{buildroot}%{_sbindir}/%{name}mon
mv %{buildroot}%{_mandir}/man1/%{collectd_name}.1* %{buildroot}%{_mandir}/man1/%{name}.1*
mv %{buildroot}%{_mandir}/man1/%{collectd_name}mon.1* %{buildroot}%{_mandir}/man1/%{name}mon.1*
mv %{buildroot}%{_mandir}/man5/%{collectd_name}-email.5* %{buildroot}%{_mandir}/man5/%{name}-email.5*
mv %{buildroot}%{_mandir}/man5/%{collectd_name}-exec.5* %{buildroot}%{_mandir}/man5/%{name}-exec.5*
mv %{buildroot}%{_mandir}/man5/%{collectd_name}-threshold.5* %{buildroot}%{_mandir}/man5/%{name}-threshold.5*
mv %{buildroot}%{_mandir}/man5/%{collectd_name}-unixsock.5* %{buildroot}%{_mandir}/man5/%{name}-unixsock.5*
mv %{buildroot}%{_mandir}/man5/%{collectd_name}.conf.5* %{buildroot}%{_mandir}/man5/%{name}.conf.5*
mv %{buildroot}%{_mandir}/man5/types.db.5* %{buildroot}%{_mandir}/man5/%{name}types.db.5*
mv %{buildroot}%{_mandir}/man5/%{collectd_name}-python* %{buildroot}%{_mandir}/man5/%{name}-python*


%clean
rm -rf %{buildroot}

%check
make check

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%doc AUTHORS COPYING ChangeLog README
%config(noreplace) %{_sysconfdir}/%{name}.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/pika
%{_unitdir}/%{name}.service
%{_sbindir}/%{name}
%{_sbindir}/%{name}mon
%{_datadir}/%{name}/types.db
%{_datadir}/%{name}/custom_types.db
%{_datadir}/%{name}/plugins/ib_bw.py
%{_datadir}/%{name}/plugins/influx_write.py
%{_datadir}/%{name}/plugins/lustre_bw.py
%{_datadir}/%{name}/plugins/*
%{_sharedstatedir}/%{name}
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/%{name}mon.1*
%{_mandir}/man5/%{name}-email.5*
%{_mandir}/man5/%{name}-exec.5*
%{_mandir}/man5/%{name}-threshold.5*
%{_mandir}/man5/%{name}-unixsock.5*
%{_mandir}/man5/%{name}.conf.5*
%{_mandir}/man5/%{name}types.db.5*

# all plugins bundled with the main collectd package
%{_libdir}/%{name}/match_empty_counter.so
%{_libdir}/%{name}/match_hashed.so
%{_libdir}/%{name}/match_regex.so
%{_libdir}/%{name}/match_timediff.so
%{_libdir}/%{name}/match_value.so
%{_libdir}/%{name}/target_notification.so
%{_libdir}/%{name}/target_replace.so
%{_libdir}/%{name}/target_scale.so
%{_libdir}/%{name}/target_set.so
%{_libdir}/%{name}/target_v5upgrade.so

%{_libdir}/%{name}/aggregation.so
%{_libdir}/%{name}/apcups.so
%{_libdir}/%{name}/cgroups.so
%{_libdir}/%{name}/check_uptime.so
%{_libdir}/%{name}/conntrack.so
%{_libdir}/%{name}/contextswitch.so
%{_libdir}/%{name}/cpu.so
%{_libdir}/%{name}/cpufreq.so
%{_libdir}/%{name}/cpusleep.so
%{_libdir}/%{name}/csv.so
%{_libdir}/%{name}/df.so
%{_libdir}/%{name}/drbd.so
%{_libdir}/%{name}/ethstat.so
%{_libdir}/%{name}/entropy.so
%{_libdir}/%{name}/exec.so
%{_libdir}/%{name}/fhcount.so
%{_libdir}/%{name}/filecount.so
%{_libdir}/%{name}/fscache.so
%{_libdir}/%{name}/hugepages.so
%{_libdir}/%{name}/infiniband.so
%{_libdir}/%{name}/interface.so
%{_libdir}/%{name}/ipc.so
%{_libdir}/%{name}/ipvs.so
%{_libdir}/%{name}/irq.so
%{_libdir}/%{name}/load.so
%{_libdir}/%{name}/logfile.so
%{_libdir}/%{name}/madwifi.so
%{_libdir}/%{name}/mbmon.so
%{_libdir}/%{name}/mcelog.so
%{_libdir}/%{name}/md.so
%{_libdir}/%{name}/mdevents.so
%{_libdir}/%{name}/memcached.so
%{_libdir}/%{name}/memory.so
%{_libdir}/%{name}/multimeter.so
%{_libdir}/%{name}/network.so
%{_libdir}/%{name}/nfs.so
%{_libdir}/%{name}/notify_nagios.so
%{_libdir}/%{name}/ntpd.so
%{_libdir}/%{name}/numa.so
%{_libdir}/%{name}/openvpn.so
%{_libdir}/%{name}/olsrd.so
%{_libdir}/%{name}/powerdns.so
%{_libdir}/%{name}/processes.so
%{_libdir}/%{name}/protocols.so
%{_libdir}/%{name}/serial.so
%{_libdir}/%{name}/statsd.so
%{_libdir}/%{name}/swap.so
%{_libdir}/%{name}/synproxy.so
%{_libdir}/%{name}/syslog.so
%{_libdir}/%{name}/table.so
%{_libdir}/%{name}/tail.so
%{_libdir}/%{name}/tail_csv.so
%{_libdir}/%{name}/tcpconns.so
%{_libdir}/%{name}/teamspeak2.so
%{_libdir}/%{name}/ted.so
%{_libdir}/%{name}/thermal.so
%{_libdir}/%{name}/threshold.so
%{_libdir}/%{name}/turbostat.so
%{_libdir}/%{name}/unixsock.so
%{_libdir}/%{name}/uptime.so
%{_libdir}/%{name}/users.so
%{_libdir}/%{name}/uuid.so
%{_libdir}/%{name}/vmem.so
%{_libdir}/%{name}/write_graphite.so
%{_libdir}/%{name}/write_log.so
%{_libdir}/%{name}/write_syslog.so
%{_libdir}/%{name}/write_tsdb.so
%{_libdir}/%{name}/buddyinfo.so
%{_libdir}/%{name}/logparser.so
%{_libdir}/%{name}/ubi.so
%{_libdir}/%{name}/write_influxdb_udp.so

#%files python
%{_mandir}/man5/%{name}-python*
%{_libdir}/%{name}/python.so

#%files disk
%{_libdir}/%{name}/disk.so

# prolog example
/etc/pika/prolog.sh

%files gpu_nvidia
%{_libdir}/%{name}/gpu_nvidia.so

%changelog
* Tue Aug 1 2023 Frank Winkler <frank.winkler@tu-dresden.de> - 5.12.0-2
- Adjustments for Rocky Linux 8 with GPU plugin. Removed all packages that are not used by PIKA.

* Thu Mar 18 2021 Sebastian Oeste <sebastian.oeste@tu-dresden.de> - 5.12.0-1
- Initial port of the spec file for pika.