#!/bin/bash

if [[ "$SLURM_JOB_CONSTRAINTS" =~ "no_monitoring" ]] ; then
    systemctl stop pika-collectd
else
    systemctl start pika-collectd
fi

exit 0
