Name:		pika-collectd-likwid-plugin
Version:	0.1
Release:	%{?release_version}%{?dist}
Summary:	The pika collectd plugin for likwid.

Group:		System Environment/Base
License:	GPL
URL:		https://gitlab.hrz.tu-chemnitz.de/pika
Source:		likwid-collectd.tar.gz

BuildRequires: pika-collectd
BuildRequires: likwid-devel
Requires: pika-collectd
Requires: likwid

%description
The pika collectd-plugin for likwid.


%prep
%setup -q -n likwid-collectd


%build
make


%install
install -Dp -m0755 likwid.so %{buildroot}/usr/lib64/pika-collectd/likwid.so


%files
/usr/lib64/pika-collectd/likwid.so


#%changelog

