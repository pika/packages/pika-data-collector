#! /bin/bash

source ../rpm.conf
DIST=el${OS_VERSION:0:1}.x86_64

get_collectd_sources() {
    local source="https://storage.googleapis.com/collectd-tarballs/collectd-5.12.0.tar.bz2"
    if ! wget --no-check-certificate -c "${source}"; then
        echo "ERROR could not fetch sources: ${source}"
        exit 1
    fi
}

build_docker_image() {
    docker build -f $(pwd)/../rpmbuild-docker/Dockerfile.plugin-rpm.${OS} --build-arg OS_VERSION=${OS_VERSION} --build-arg DIST=${DIST} --build-arg LIKWID_VERSION=${LIKWID_VERSION} --build-arg RELEASE_VERSION=${RELEASE_VERSION} -t collectd-likwid-plugin-build "$(pwd)/../rpmbuild-docker/"
}

run_build() {
    build_docker_image
    docker run --rm --volume="$(pwd)":/src --workdir=/src collectd-likwid-plugin-build
}

check_rpm_deps() {
    [[ -f pika-collectd-5.12.0-${RELEASE_VERSION}.${DIST}.rpm ]] && \
        [[ -f likwid-${LIKWID_VERSION}-${RELEASE_VERSION}.${DIST}.rpm ]] && \
        [[ -f likwid-devel-${LIKWID_VERSION}-${RELEASE_VERSION}.${DIST}.rpm ]] || echo "Error RPM dependencies not satisfied!"
}

check_rpm_deps || exit 1
if [[ ! -d collectd-5.12.0 ]]; then
    get_collectd_sources
    tar xjf collectd-5.12.0.tar.bz2
fi
run_build
