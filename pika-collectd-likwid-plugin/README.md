# pika-collectd-likwid-plugin

## Build RPM package

You need to build the `pika-collectd` and `pika-likwid` packages before.
Afterwards, copy the resulting RPM files:

    collectd-5.12.0-1.el8.x86_64.rpm
    likwid-5.2.2-1.x86_64.rpm
    likwid-devel-5.2.2-1.x86_64.rpm

… in this directory. Then, to build the pika-collectd-likwid-plugin RPM just run.

```
./build.sh
```
