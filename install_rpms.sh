#! /bin/bash


source ./rpm.conf
DIST=el${OS_VERSION:0:1}.x86_64

# clean unused images
docker system prune -a -f

cd rpmbuild-docker
# adjust release version for rpm builds
sed -i "s/RELEASE_VERSION=.*/RELEASE_VERSION=${RELEASE_VERSION}/" docker-rpm-build.sh
docker build -f Dockerfile.${OS}${OS_VERSION%%.*} --build-arg OS_VERSION=${OS_VERSION} -t rpmbuild .
cd ..

cd pika-collectd
./build.sh
cd ..

cd pika-likwid
./build.sh
cd ..

cd pika-collectd-likwid-plugin
cp ../pika-collectd/RPMS/x86_64/pika-collectd-5.12.0-${RELEASE_VERSION}.${DIST}.rpm .
cp ../pika-likwid/RPMS/x86_64/likwid-${LIKWID_VERSION}-${RELEASE_VERSION}.${DIST}.rpm .
cp ../pika-likwid/RPMS/x86_64/likwid-devel-${LIKWID_VERSION}-${RELEASE_VERSION}.${DIST}.rpm .
./build.sh
cd ..

# collect all rpms
if [ -d "RPMS" ]; then rm -rf RPMS; fi
mkdir RPMS

# pika-collectd
cp pika-collectd/RPMS/x86_64/pika-collectd-5.12.0-${RELEASE_VERSION}.${DIST}.rpm RPMS
cp pika-collectd/RPMS/x86_64/pika-collectd-gpu_nvidia-5.12.0-${RELEASE_VERSION}.${DIST}.rpm RPMS

# pika-likwid
cp pika-likwid/RPMS/x86_64/likwid-${LIKWID_VERSION}-${RELEASE_VERSION}.${DIST}.rpm RPMS

# pika-collectd-likwid-plugin
cp pika-collectd-likwid-plugin/rpmbuild/RPMS/x86_64/pika-collectd-likwid-plugin-0.1-${RELEASE_VERSION}.${DIST}.rpm RPMS
