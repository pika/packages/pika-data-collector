# pika-likwid

Create the likwid RPM package for PIKA, just run.

```sh
./build.sh
```

## PIKA Perfgroups for LIKWID

PIKA uses its own LIKWID perfgroups to avoid or minimize multiplexing of hardware counters. 
The collected metrics are 
* FLOPS (flops_sp, flops_dp), 
* IPC (ipc), 
* main memory bandwidth (mem_bw) and 
* power consumption (rapl_power).

PIKA perfgroup files are named *pika_metrics_#.txt*, where *#* being a number (see pika_metrics_#.txt in [perfgroups/](perfgroups/)*CPU_ARCH*).

### What should I do if no PIKA groups exist for my CPU architecture yet?
If no pika perfgroup files exist for a CPU architecture, they can be derived from existing [LIKWID perfgroups](https://github.com/RRZE-HPC/likwid/tree/master/groups). 

Once LIKWID has been successfully installed, your current architecture can be queried using:

```sh
$ likwid-perfctr --info
--------------------------------------------------------------------------------
CPU name:	Intel(R) Xeon(R) Platinum 8470
CPU type:	Intel SapphireRapids processor
CPU clock:	2.00 GHz
CPU family:	6
CPU model:	143
CPU short:	SPR
```

In this example the current architecure is `SPR` (CPU short). 
All metrics are located in the folder likwid/groups/[CPU short]. These must now be rewritten to the PIKA group files. Please note that no unit prefixes are saved in PIKA. Therefore, for example, Mega (1.0E-06) must be removed from FLOPS.


IPC or CPI (the inverse) is defined in almost every LIKWID group, because it is provided by a fixed counter register.

To determine the FLOPS there are usually several predefined groups for the different types of FLOPS. 
Underlying counters are usually obtained via programmable counter registers, which are only available in limited quantity. 
Since PIKA summarizes all types of FLOPS into a single FLOPS value (*flops_any*), normalized to single precision, it might be possible to get such a value with only one perfgroup. 
If this is not possible multiple PIKA perfgroup files have to define the individual FLOPS types, e.g. *flops_dp*, *flops_sp*, *flops_avx*, which are then summarized by our collectd LIKWID plugin. 

Main memory bandwidth and power consumption are often per-socket counters (at least for Intel CPUs). Predefined LIKWID perfgroups are called *MEM* and *ENERGY*. 
For PIKA, we aggregate read and write bandwidth into one value. 

### Additional CPU Architecture Information
For Zen and Zen2 the fixed counters (FIX*) are broken. Thus, we use the PMC counters to determine the IPC. Zen has four PMC counters. Zen2 has six PMC counters.

## Per-Core or Per-Socket Metric?

To determine whether a metric is per core or per socket, only a test run helps. 
For example, use a benchmark from [LIKWID bench](https://github.com/RRZE-HPC/likwid/wiki/Likwid-Bench) and measure the counters with *likwid-perfctr*:

    $ likwid-perfctr -g FLOPS likwid-bench -t triad_avx -w S0:128KB -w S1:128KB

The given example measures the LIKWID perfgroup *FLOPS* and executes the AVX version of the triad benchmark on socket *0* and *1* and thus on all threads in a dual-socket compute node. 
If all but two threads show a value of *0*, the metric is per-socket. 
List such metrics (comma-separated) with the option *PerSocketMetrics* in the LIKWID plugin block of the [collectd configuration file](../pika-collectd/pika-collectd.conf) to avoid sending unwanted zero values to the database.

If everything works, copy the new architecture folder with the PIKA group files to the perfgroups folder and rebuild the RPM package.
After that, the [pika-collectd-likwid-plugin](../pika-collectd-likwid-plugin) also has to be rebuilt.
