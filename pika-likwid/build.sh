#! /bin/bash

source ../rpm.conf

build_docker_image() {
    if [[ $(docker images -q rpmbuild) == "" ]]; then
        # adjust release version for rpm builds
        sed -i "s/RELEASE_VERSION=.*/RELEASE_VERSION=${RELEASE_VERSION}/" ../rpmbuild-docker/docker-rpm-build.sh
        docker build -f $(pwd)/../rpmbuild-docker/Dockerfile.${OS} --build-arg OS_VERSION=${OS_VERSION} -t rpmbuild "$(pwd)/../rpmbuild-docker/"
    fi
}

get_source() {
    local source="https://github.com/RRZE-HPC/likwid/archive/refs/tags/v${LIKWID_VERSION}.tar.gz"
    if ! wget --no-check-certificate -c "${source}"; then
        echo "ERROR could not fetch sources: ${source}"
        exit 1
    fi
}

run_docker_rpmbuild() {
    build_docker_image
    docker run --env-file ../rpm.conf --rm --volume="$(pwd)":/src --workdir=/src rpmbuild likwid.spec
}

get_source
run_docker_rpmbuild
