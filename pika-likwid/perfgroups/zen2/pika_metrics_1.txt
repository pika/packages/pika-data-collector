SHORT PIKA metric group 1

EVENTSET
PMC0  RETIRED_INSTRUCTIONS
PMC1  CPU_CLOCKS_UNHALTED
DFC0  DRAM_CHANNEL_0
DFC1  DRAM_CHANNEL_1

METRICS
ipc  PMC0/PMC1
mem_bw (DFC0+DFC1)*(4.0/(num_numadomains/num_sockets))*64.0/time

LONG
--
The fixed counter registers are broken. Thus, we use PMC registers to determine
the IPC.
-
Ryzen implements the RAPL interface previously introduced by Intel.
This interface enables to monitor the consumed energy on the core and package
domain. It is not documented by AMD which parts of the CPU are in which domain.
For now, we ignoring PWR0  RAPL_CORE_ENERGY.
-
Profiling group to measure memory bandwidth drawn by all cores of a socket.
Since this group is based on Uncore events it is only possible to measure on a
per socket base.
The group provides almost accurate results for the total bandwidth
and data volume.
The metric formulas contain a correction factor of (4.0/(num_numadomains/num_sockets)) to
return the value for all 4 memory controllers in NPS1 mode per socket. This is probably
a work-around. Requested info from AMD but no answer.
