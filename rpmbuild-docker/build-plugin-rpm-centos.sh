#! /bin/bash

set -x

yum --nogpgcheck localinstall -y pika-collectd-5.12.0-${RELEASE_VERSION}.${DIST}.rpm likwid-${LIKWID_VERSION}-${RELEASE_VERSION}.${DIST}.rpm likwid-devel-${LIKWID_VERSION}-${RELEASE_VERSION}.${DIST}.rpm
make rpm
