# PIKA data collector

The PIKA data collector is a daemon that must be installed on each compute node.
It consists of the following components.

- pika-collectd - A patched version of collectd.
- pika-likwid - A patched version of the Likwid performance monitoring and benchmark suite.
- pika-collectd-likwid-plugin - Our additional collectd-likwid-plugin to access Likwid performance data.

We provide separate RPM packages for all components.
In order to build the RPM packages we provide a generic docker container which is used for the creation of subsequent packages.

**Hint:** If you encounter permission problems during the installtion, set `umask 0002` before `git clone` and keep it for the entire build session or set it again for subsequent changes.

Make sure [Docker Engine](https://docs.docker.com/get-docker/) is installed as well.

```
umask 0002
git clone https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-data-collector.git
```

First, copy `rpm_template.conf` to `rpm.conf` and modify it according to your settings.

```
cp rpm_template.conf rpm.conf

vi rpm.conf
OS=rockylinux #or centos
OS_VERSION=8.7
LIKWID_VERSION=5.3.0
```

You can either build the RPM packages step by step or use a prepared all-in-one installation script for [all components](#pika-collectd-with-all-plugins).
Please see also the instructions in the README files of the components.

1. [pika-collectd](#pika-collectd)
2. [pika-likwid](#pika-likwid)
3. [pika-collectd-likwid-plugin](#pika-collectd-likwid-plugin)
4. [Configuration on target system](#configuration-on-target-system)

## pika-collectd

Build the collectd component of PIKA. Details [here](pika-collectd/README.md).

```
cd pika-collectd
cp pika-collectd-template.conf pika-collectd.conf
./build.sh
```

If the build was successful the resulting RPM packages should be placed under `pika-collectd/RPMS/x86_64/`.

Then install the package `pika-collectd-5.12.0-1.*.rpm` on your target system.

```
dnf localinstall ./RPMS/x86_64/pika-collectd-5.12.0-1.*.rpm
```

If performance data from NVIDIA GPUs is to be recorded, the pika-collectd-gpu_nvidia-5.12.0-1.*.rpm package must be installed.
Make sure that libnvidia-ml.so is available, e.g. you can put it in your library path.

```
dnf localinstall ./RPMS/x86_64/pika-collectd-gpu_nvidia-5.12.0-1.*.rpm
```

## pika-likwid

To record the hardware performance counters provided by Likwid, your architecture must be one of the following:

- broadwellEP
- CLX
- haswellEP
- sandybridgeEP
- SPR
- zen
- zen2
- zen4

If your architecture is not listed here, follow the instructions [here](pika-likwid) to add pika metrics for your current architecture.

Build the likwid component of PIKA.

```
cd pika-likwid
./build.sh
```

If the build was successful the resulting RPM packages should be placed under `pika-likwid/RPMS/x86_64/`.

Then install the package `likwid-5*.rpm` for likwid on your target system.

```
dnf localinstall ./RPMS/x86_64/likwid-5*.rpm
```

## pika-collectd-likwid-plugin

Build the pika-collectd-likwid-plugin.

```
cd pika-collectd-likwid-plugin
```

We need to copy our build dependencies generated in the previous steps into the directory.

```
cp ../pika-collectd/RPMS/x86_64/pika-collectd-5.12.0-1.*.rpm ../pika-likwid/RPMS/x86_64/likwid-5*.rpm ../pika-likwid/RPMS/x86_64/likwid-devel-*.rpm ./
./build.sh
```

If the build was successful the resulting RPM package should be placed under `pika-collectd-likwid-plugin/rpmbuild/RPMS/x86_64/`.

Then install the package `pika-collectd-likwid-plugin-0.1-1.*.rpm` for the pika-collectd-likwid-plugin on your target system.

```
dnf localinstall ./rpmbuild/RPMS/x86_64/pika-collectd-likwid-plugin-0.1-1.*.rpm
```

## pika-collectd with all plugins

First create a configuration file for [pika-collectd](pika-collectd/README.md) and then build all RPM packages.

```
cd pika-collectd
cp pika-collectd-template.conf pika-collectd.conf

vi pika-collectd.conf
...
cd ..
./install_rpms.sh
```

The following packages should then be available in the RPMS folder and installed in the following order:

```
cd RPMS
dnf localinstall pika-collectd-5.12.0-1.*.rpm -y
dnf localinstall likwid-5*.rpm -y
dnf localinstall pika-collectd-likwid-plugin-0.1-1.*.rpm -y

# optional if GPUs are available
dnf localinstall pika-collectd-gpu_nvidia-5.12.0-1.*.rpm
```

**Hint:** If performance data from NVIDIA GPUs is to be recorded, the pika-collectd-gpu_nvidia-5.12.0-1.*.rpm package must be installed.
Make sure that libnvidia-ml.so is available, e.g. you can put it in your library path.

## Configuration on target system

After the installation of the RPM package as mentioned above ...

Our pika-collectd is talking to influxdb via a python package therefore it is necessary to have `python3-influxdb` installed on the system.
Since there is no package available for Centos Linux based systems you might need to install that via pip.

```
pip3 install influxdb
```

In terms of Rocky Linux based systems it will be automatically installed.

Furthermore, PIKA creates a **logrotate** configuration for /var/log/pika-collectd.log.
The current configuration is:

```
vi /etc/logrotate.d/pika

/var/log/pika-collectd.log {
su root root

# rotate log files daily
daily

# keep 14 days worth of backlogs
rotate 12

# use date as a suffix of the rotated file
dateext

}
```

Feel free to customise these settings according to your needs.

### pika-collectd configure influxdb credentials

Further, it is necessary to configure your login credentials for influxdb in the `pika-collectd.conf`.
Adjust `/etc/pika-collectd.conf` in the influx section for the following lines.

e.g.

```
host "127.0.0.1"
port "8086"
user "admin"
pwd "YOUR-PASSWORD"
database "pika"
```

If performance data from NVIDIA GPUs is to be recorded, you also have to comment in `gpu_nvidia` in `pika-collectd.conf` (lines 78-85, 221-233, and 236-284).

### pika-collectd ib_bw plugin

Configure devices and directory for Infiniband counter in `pika-collectd.conf`.

### pika-collectd lustre_bw plugin

Configure lustre path instances in `pika-collectd.conf`.
